This is the "Hanabi" part of the board game "Hanabi &amp; Ikebana".

I strongly recommend you check out the [game review on
YouTube](http://www.youtube.com/watch?v=pYBGnNrdYe4) so you know better what I am talking about in
the following.


###Implementation

Implemented with web technologies, the game can easily be played on multiple hardware and software
platforms --- PC (Windows, Mac, Linux etc) and mobile devices (Android, iOS etc).

When players visit the web client (from browser), they should see the UI in HTML and start the game
from there.  All subsequent communications are done with websockets (Yes, not AJAX, because I hate
it.  Plus its inefficiency as it establishes new HTTP connections each time data is sent.  Yet
another reason is that I don't want this game run on IE). Of course, at the same time, a websocket
server (written with [pywebsocket](http://code.google.com/p/pywebsocket/)) is running and taking
care of game logic.


###How to setup

####Server
* launch a web server and serve the HTMLs and Javascripts.
* launch the pywebsocket standalone server. Note to specify the "websocket handlers directory" with
  "-w" option.

####Client
* launch a web browser (preferrably Google Chrome / Chromium / Firefox / Safari) and visit the URL
  that serves <code>hanabi.html</code>, and start from there.
