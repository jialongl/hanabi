#!/bin/bash

OLD_PID=$(pgrep -f standalone.py)
[[ "x$OLD_PID" != "x" ]] && kill $OLD_PID

#PYWS_DEBUG=" --log-level debug"
export PYTHONPATH=$(readlink -f $(dirname $0)/src/pywebsocket-handlers/)
python /usr/lib/python2.7/site-packages/mod_pywebsocket/standalone.pyc -p 9876 -d $PYTHONPATH $PYWS_DEBUG &
