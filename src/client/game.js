function listRooms() {
	var m = {'type': HM_LIST_ROOMS};
	ws.send(JSON.stringify(m));
}

function createRoom(roomName) {
	var m = {'type': HM_CREATE_ROOM,
	         'client_id': clientId,
	         'room_name': roomName};
	ws.send(JSON.stringify(m));
}

function joinRoom(roomId) {
	var m = {'type': HM_JOIN_ROOM,
	         'client_id': clientId,
	         'room_id': roomId};
	ws.send(JSON.stringify(m));
}

function startGame() {
	var m = {'type': HM_START_GAME,
	         'client_id': clientId};
	ws.send(JSON.stringify(m));
	addToLog("Game started.");
}

function playCard(cardId) {
	var m = {'type': HM_PLAY_CARD,
	         'client_id': clientId,
	         'card_id': cardId};
	ws.send(JSON.stringify(m));
	addToLog("Played the card with id: " + cardId);
}

function discardCard(cardId) {
	var m = {'type': HM_DISCARD_CARD,
	         'client_id': clientId,
	         'card_id': cardId};
	ws.send(JSON.stringify(m));
	addToLog("Discarded the card with id: " + cardId);
}

function tellPeer(peerId, msg) {
	var m = {'type': HM_TELL_PEER,
	         'client_id': clientId,
	         'peer_id': peerId,
	         'msg': msg};
	ws.send(JSON.stringify(m));
	addToLog("Told peer: " + peerId + " msg: " + msg);
}
