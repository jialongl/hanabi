// pre-game request messages
var HM_GET_MY_INFO = 11
var HM_CREATE_ROOM = 12
var HM_JOIN_ROOM   = 13
var HM_LIST_ROOMS  = 14
var HM_START_GAME  = 15

// pre-grame response messages
var HMR_JOIN_NONEXISTENT_ROOM = 25
var HMR_JOIN_CURRENT_ROOM     = 26
var HMR_JOIN_ROOM_SUCCESS     = 27

// in-game messages
var HM_INIT_CARDS   = 17
var HM_MY_TURN      = 18
var HM_PLAY_CARD    = 19
var HM_DRAW_CARD    = 22
var HM_DISCARD_CARD = 20
var HM_TELL_PEER    = 21

var HM_TP_REDS    = 40
var HM_TP_YELLOWS = 41
var HM_TP_GREENS  = 42
var HM_TP_BLUES   = 43
var HM_TP_BLACKS  = 44
var HM_TP_COLORS  = 45
var HM_TP_ONES    = 46
var HM_TP_TWOS    = 47
var HM_TP_THREES  = 48
var HM_TP_FOURS   = 49
var HM_TP_FIVES   = 50

// card colors/suites
var HC_SUITE_RED    = 29
var HC_SUITE_YELLOW = 30
var HC_SUITE_GREEN  = 31
var HC_SUITE_BLUE   = 32
var HC_SUITE_BLACK  = 33
var HC_SUITE_COLOR  = 34



var ws = null;
var logBox = null;
var clientId = -1;
var roomId = -1;
var ipAddr = "";
var portNumber = -1;
var cardsInHands = [];


function addToLog(line) {
	var d = new Date();
	line = d.toTimeString() + ":\t" + line;
	console.log(line);
	logBox.value += line + "\n";
	logBox.scrollTop = 1000000;
}

function documentOnload() {
	var protocol = window.location.protocol;
	var scheme = (protocol == 'https:') ? 'wss://' : 'ws://';
	var portNumber = 9876;
	var hostname = window.location.host.split(':')[0];
	if (protocol == 'file:' && hostname.length == 0)
		hostname = 'localhost';
	hanabiWsLocation = scheme + hostname + ':' + portNumber + '/hanabi';

	logBox = document.getElementById('log');
}
