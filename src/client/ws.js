function initWs() {
	if ('WebSocket' in window) {
		ws = new WebSocket(hanabiWsLocation);

	} else if ('MozWebSocket' in window) {
		addToLog('Fall back to MozWebSocket.');
		ws = new MozWebSocket(hanabiWsLocation);

	} else {
		addToLog('WebSocket is not available.');
		return ;
	}

	ws.onopen = function () {
		addToLog('WebSocket opened');
		var m = {"type": HM_GET_MY_INFO};
		ws.send(JSON.stringify(m));
	};
	ws.onmessage = function (event) {
		var m = JSON.parse(event.data);

		switch (m['type']) {
		case HM_MY_TURN:
			addToLog("Your turn!!!");
			break;

		case HM_GET_MY_INFO:
			clientId = m['_id'];
			ipAddr = m['_ip_addr'];
			portNumber = m['_port_number'];
			break;

		case HM_CREATE_ROOM:
			roomId = m['room_id'];
			addToLog("Created and joined room " + roomId + ": \"" + m['room_name'] + "\".");
			break;

		case HM_LIST_ROOMS:
			var s = "";
			for (var r in m['rooms']) {
				s += "room id: " + m['rooms'][r]['id'] + ", name: " + m['rooms'][r]['name'];
				if (m['rooms'][r]['id'] == roomId)
					s += " (in here)";
				s += "\n";
			}
			addToLog(s);
			break;

		case HM_PLAY_CARD:
			if (m['client_id'] == clientId) {
				addToLog('something went wrong --- get a echo of HM_PLAY_CARD');
			}
			else {
				// remove card_id from	that client's array
				for (var i = 0; i < cardsInHands[m['client_id']].length; i++) {
					if (cardsInHands[m['client_id']][i]['_id'] == m['card_id'])
						break;
				}
				cardsInHands.splice(i, 1);
			}
			break;

		case HM_DISCARD_CARD:
			break;

		case HM_TELL_PEER:
			break;

		case HM_DRAW_CARD:
			cardsInHands[m['client_id']].push(m['card']);
			break;

		case HMR_JOIN_ROOM_SUCCESS:
			roomId = m['room_id'];
			addToLog("Joined room " + roomId);
			break;

		case HMR_JOIN_NONEXISTENT_ROOM:
			addToLog("Room " + m["room_id"] + " doesn't exist.");
			break;

		case HMR_JOIN_CURRENT_ROOM:
			addToLog("You are already in room " + m["room_id"]);
			break;

		case HM_INIT_CARDS:
			cardsInHands = m['cards'];

			break;
		}
	};
	ws.onerror = function () {
		addToLog('WebSocket error!');
	};
	ws.onclose = function (event) {
		var logMessage = 'WebSocket closed (';
		if ((arguments.length == 1) &&
		    ('CloseEvent' in window) &&
		    (event instanceof CloseEvent)) {
			logMessage += 'wasClean = ' + event.wasClean;

		} else {
			logMessage += 'CloseEvent is not available';
		}
		addToLog(logMessage + ')');
	};
}

function uninitWs() {
	if (!ws) {
		addToLog('Not connected');
		return;
	}

	ws.close();
}
