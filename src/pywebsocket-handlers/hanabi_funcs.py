from hanabi_objs import *
import random
import copy
import os
import json

def json_encode_object_compatible (obj):
    return obj.__dict__


def init_cards ():
    cards = []
    card_id = 0

    # init normal cards
    for suite in range(HanabiCard.HC_SUITE_RED, HanabiCard.HC_SUITE_COLOR):
        for i in range(3):                                  # 3 ones
            cards.append( HanabiCard(card_id, suite, 1) )
            card_id += 1

        for rank in range(2, 5):
            for i in range(2):                              # 2 twos, 2 threes, 2 fours
                cards.append( HanabiCard(card_id, suite, rank) )
                card_id += 1

        cards.append( HanabiCard(card_id, suite, 5) )       # 1 five
        card_id += 1

    # init color cards
    for rank in range(1, 6):
        cards.append( HanabiCard(card_id, HanabiCard.HC_SUITE_COLOR, rank) )
        card_id += 1

    return cards


def shuffle_cards (cards):
    for i in range(50):
        random.shuffle(cards, random.random)



# Send card info to clients for their initializations.
# @param client : a HanabiClient instance
#        cards  : a dict of (list of HanabiCards), indexed by client_id.
def send_cards_for_client_init (client, cards):
    m = {'type': HanabiMessage.HM_INIT_CARDS, 'cards': cards}
    a = json.dumps(m, default = json_encode_object_compatible)
    client._ws_stream.send_message(a, binary=isinstance(a, unicode))


# The game logic. It is the thread's target which allows multiple game sessions to run concurrently.
# @param room : a HanabiRoom instance
#
# Primarily, the "_clients" and "_pipe" attributes are used.
# Check out HanabiRoom's definition for details.
def start_game (room):

    n_tokens = 8
    n_lives  = 3

    # init, shuffle and distribute cards.
    cards_in_deck = init_cards()
    cards_in_hands = {}
    cards_played = { HanabiCard.HC_SUITE_RED:    [],
                     HanabiCard.HC_SUITE_YELLOW: [],
                     HanabiCard.HC_SUITE_GREEN:  [],
                     HanabiCard.HC_SUITE_BLUE:   [],
                     HanabiCard.HC_SUITE_BLACK:  [],
                     HanabiCard.HC_SUITE_COLOR:  [] }
    cards_discarded = copy.deepcopy(cards_played)

    shuffle_cards(cards_in_deck)

    clients = room._clients
    for c in clients:
        cards_in_hands[c._id] = cards_in_deck[:5]
        cards_in_deck = cards_in_deck[5:]

    for c in clients:
        send_cards_for_client_init(c, cards_in_hands)

    # set up IPC with the main thread
    r, w = room._pipe
    rp = os.fdopen(r, 'r', 0)

    # Decide who to start first, and inform him (in the while loop).
    current_client_index = random.randrange(len(clients))

    # loop until win or lose
    win = False
    lose = False
    while not win and not lose:
        m = {'type' : HanabiMessage.HM_MY_TURN}
        a = json.dumps(m)
        clients[current_client_index]._ws_stream.send_message(a, binary=isinstance(a, unicode))

        line = rp.readline()
        if not line:
            break
        elif line == IPCMessage.CLIENT_FORCIBLY_DISCONNECTS:
            break
        else:
            m = json.loads(line)


        if clients[current_client_index]._id != m['client_id']:
            continue


        if m['type'] == HanabiMessage.HM_PLAY_CARD:
            # sanitize card id --- make sure the player indeed has the card


            # inform every other client that this card is played
            a = json.dumps(m)
            for c in clients:
                if c._id != m['client_id']:
                    c._ws_stream.send_message(a, binary=isinstance(a, unicode))

            # remove the card from cards_in_hands[m['client_id']], draw a new card
            for card in cards_in_hands[m['client_id']]:
                if card._id == m['card_id']:
                    cards_in_hands[m['client_id']].remove(card)

                    new_card = cards_in_deck.pop(0)
                    cards_in_hands[m['client_id']].append(new_card)
                    m = {'type': HanabiMessage.HM_DRAW_CARD,
                         'client_id': clients[current_client_index]._id,
                         'card': vars(new_card)}
                    a = json.dumps(m)
                    for c in clients:
                        c._ws_stream.send_message(a, binary=isinstance(a, unicode))
                    break

            # compute a var necessary to check if "card" is playable
            max_rank = 0
            for c in cards_played[card._suite]:
                if c._rank > max_rank:
                    max_rank = c._rank

            # if this card is OK to play, add the card to "cards_played"
            # else discard the card and n_lives--
            if card._rank == (max_rank + 1):
                cards_played[card._suite].append(card)
            else:
                cards_discarded[card._suite].append(card)
                n_lives -= 1
                if n_lives == 0:
                    lose = True


        elif m['type'] == HanabiMessage.HM_DISCARD_CARD:
            # sanitize card id --- make sure the player indeed has the card

            # inform every other client that this card is discarded
            a = json.dumps(m)
            for c in clients:
                if c._id != m['client_id']:
                    c._ws_stream.send_message(a, binary=isinstance(a, unicode))

            # remove the card from cards_in_hands[m['client_id']], draw a new card
            for card in cards_in_hands[m['client_id']]:
                if card._id == m['card_id']:
                    cards_in_hands[m['client_id']].remove(card)
                    cards_discarded[card._suite].append(card)

                    new_card = cards_in_deck.pop(0)
                    cards_in_hands[m['client_id']].append(new_card)
                    m = {'type': HanabiMessage.HM_DRAW_CARD,
                         'client_id': clients[current_client_index]._id,
                         'card': vars(new_card)}
                    a = json.dumps(m)
                    for c in clients:
                        c._ws_stream.send_message(a, binary=isinstance(a, unicode))
                    break

            n_tokens += 1


        elif m['type'] == HanabiMessage.HM_TELL_PEER:
            if n_tokens == 0:
                continue

            n_tokens -= 1

            # inform every other client that A told B something
            a = json.dumps(m)
            for c in clients:
                if c._id != m['client_id']:
                    c._ws_stream.send_message(a, binary=isinstance(a, unicode))


        # move on to the next player
        if current_client_index == len(clients) - 1:
            current_client_index = 0
        else:
            current_client_index += 1


    # game thread ends; clean up.
    rp.close()
    os.close(w)
    room._pipe = None
