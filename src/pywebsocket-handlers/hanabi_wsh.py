#!/usr/bin/python
# pywebsocket server handlers for the main game logic.

from mod_pywebsocket import common
from threading import Thread
import json
import os
from hanabi_objs import *
from hanabi_funcs import *


clients = {}
rooms = {}


# ---------------------------- pywebsocket handlers ----------------------------

def web_socket_do_extra_handshake (request):
    ip_addr, port_number, a, b = request.connection.remote_addr
    print 'New connection from %r.' % (request.connection.remote_addr,)

    c = HanabiClient(ip_addr, port_number)
    clients[c._id] = c




def web_socket_transfer_data (request):

    while True:
        m_str = request.ws_stream.receive_message()
        if m_str is None:
            return

        m = json.loads(m_str)

        if m['type'] == HanabiMessage.HM_GET_MY_INFO:
            ip_addr, port_number, a, b = request.connection.remote_addr

            for c in clients.values():
                if c._ip_addr == ip_addr and c._port_number == port_number:
                    m.update(vars(c))
                    a = json.dumps(m)
                    request.ws_stream.send_message(a, binary=isinstance(a, unicode))

                    c._ws_stream = request.ws_stream
                    break


        elif m['type'] == HanabiMessage.HM_CREATE_ROOM:
            client = clients[m['client_id']]
            r = HanabiRoom(room_name = m['room_name'], owner = client)
            rooms[r._id] = r

            m['room_id'] = r._id
            del m['client_id']
            a = json.dumps(m)
            request.ws_stream.send_message(a, binary=isinstance(a, unicode))


        elif m['type'] == HanabiMessage.HM_JOIN_ROOM:
            client_id = m['client_id']
            room_id = int(m['room_id'])
            client = clients[client_id]

            if not rooms.has_key(room_id):
                m['type'] = HanabiMessage.HMR_JOIN_NONEXISTENT_ROOM
                del m['client_id']
                a = json.dumps(m)
                client._ws_stream.send_message(a, binary=isinstance(a, unicode))
                continue

            if client._room_id != -1:                       # remove player from the old room
                if room_id == client._room_id:
                    m['type'] = HanabiMessage.HMR_JOIN_CURRENT_ROOM
                    del m['client_id']
                    a = json.dumps(m)
                    client._ws_stream.send_message(a, binary=isinstance(a, unicode))
                    continue


                old_room = rooms[client._room_id]
                old_room._clients.remove(client)

                if len(old_room._clients) == 0:             # remove the room if he leaves last
                    del rooms[client._room_id]

            client._room_id = room_id

            new_room = rooms[room_id]
            new_room._clients.append(client)

            m['type'] = HanabiMessage.HMR_JOIN_ROOM_SUCCESS
            del m['client_id']
            a = json.dumps(m)
            client._ws_stream.send_message(a, binary=isinstance(a, unicode))


        elif m['type'] == HanabiMessage.HM_LIST_ROOMS:
            m = {'type': HanabiMessage.HM_LIST_ROOMS,
                 'rooms': [{'id': r._id, 'name': r._name} for r in rooms.values()]}
            a = json.dumps(m, default = json_encode_object_compatible)
            request.ws_stream.send_message(a, binary=isinstance(a, unicode))


        elif m['type'] == HanabiMessage.HM_START_GAME:
            client_id = m['client_id']
            client = clients[client_id]
            room = rooms[client._room_id]

            if len(room._clients) == 2:
                if not hasattr(room, '_pipe') or not room._pipe:
                    room._pipe = os.pipe()                  # create a pipe for IPC later
                Thread(target = start_game, args = (room, )).start()


        # in-game messages are dispatched to "start_game" thread, using the pipe.
        elif HanabiMessage.HM_MY_TURN <= m['type'] <= HanabiMessage.HM_TELL_PEER:
            client = clients[m['client_id']]
            room = rooms[client._room_id]
            os.write(room._pipe[1], m_str + "\n")




def web_socket_passive_closing_handshake(request):
    print 'Closing connection for %r.' % (request.connection.remote_addr,)

    a,b,d,e = request.connection.remote_addr
    for c_id in clients.keys():
        c = clients[c_id]
        if c._ip_addr == a and c._port_number == b:
            del clients[c_id]
            break

    # "Kick" all clients out of the room if one disconnects during a game;
    # Leave things as they are if one client disconnects:
    #     from a room before a game could start;
    #     from the server outside of any rooms;
    if c._room_id != -1:
        r = rooms[c._room_id]
        del rooms[c._room_id]

        if hasattr(r, '_pipe') and r._pipe is not None:
            os.write(r._pipe[1], IPCMessage.CLIENT_FORCIBLY_DISCONNECTS)
        else:
            for c1 in r._clients:
                c1._room_id = -1


    code, reason = request.ws_close_code, request.ws_close_reason
    # pywebsocket sets pseudo code for receiving an empty body close frame.
    if code == common.STATUS_NO_STATUS_RECEIVED:
        code = None
        reason = ''

    return code, reason
