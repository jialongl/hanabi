import json
from threading import Lock

client_id = 99
mutex_c = Lock()

room_id = 147
mutex_r = Lock()

def generate_client_id ():
    global client_id
    mutex_c.acquire()
    cid = client_id
    client_id += 1
    mutex_c.release()
    return cid


def generate_room_id ():
    global room_id
    mutex_r.acquire()
    rid = room_id
    room_id += 1
    mutex_r.release()
    return rid


class HanabiClient (object):
    """A data class that represents connected clients."""

    # instance vars to be set after instantiation:
    # _ws_stream : the interface through which to send/recv data to/from the client

    def __init__(self, ip_addr, port_number):
        self._id = generate_client_id()
        self._room_id = -1
        self._ip_addr = ip_addr
        self._port_number = port_number


class HanabiRoom (object):
    """A data class that encapsulates clients in the same game."""

    # instance vars to be set after instantiation:
    # _clients : a list of HanabiClients in this room.
    # _pipe    : a tuple of fds returned by os.pipe(),
    #            used for IPC with web_socket_transfer_data().

    def __init__(self, room_name, owner):
        self._id = generate_room_id()
        self._name = room_name
        self._clients = []
        self._clients.append(owner)
        owner._room_id = self._id


class HanabiMessage (object):
    """A data class that represents messages between clients and server."""

    # pre-game request messages
    HM_GET_MY_INFO = 11
    HM_CREATE_ROOM = 12
    HM_JOIN_ROOM   = 13
    HM_LIST_ROOMS  = 14
    HM_START_GAME  = 15

    # pre-game response messages
    HMR_JOIN_NONEXISTENT_ROOM = 25
    HMR_JOIN_CURRENT_ROOM     = 26
    HMR_JOIN_ROOM_SUCCESS     = 27

    # in-game messages
    HM_INIT_CARDS   = 17
    HM_MY_TURN      = 18
    HM_PLAY_CARD    = 19
    HM_DRAW_CARD    = 22
    HM_DISCARD_CARD = 20
    HM_TELL_PEER    = 21


    def __init__(self, message_type):
        # obviously, "message_type" takes one value from above.
        self._type = message_type


class IPCMessage (object):
    """A data class for Inter-Process Communication messages."""

    CLIENT_FORCIBLY_DISCONNECTS = "9c6bf785977883e4\n"


class HanabiCard (object):

    HC_SUITE_RED    = 29
    HC_SUITE_YELLOW = 30
    HC_SUITE_GREEN  = 31
    HC_SUITE_BLUE   = 32
    HC_SUITE_BLACK  = 33
    HC_SUITE_COLOR  = 34

    def __init__(self, card_id, suite, rank):
        self._id = card_id
        self._suite = suite
        self._rank = rank

    def __repr__(self):
        return json.dumps(self.__dict__)
